"use strict";
var _createClass = function() {
    function e(e, t) {
        for (var i = 0; i < t.length; i++) {
            var o = t[i];
            o.enumerable = o.enumerable || !1,
            o.configurable = !0,
            "value"in o && (o.writable = !0),
            Object.defineProperty(e, o.key, o)
        }
    }
    return function(t, i, o) {
        return i && e(t.prototype, i),
        o && e(t, o),
        t
    }
}();
function _classCallCheck(e, t) {
    if (!(e instanceof t))
        throw new TypeError("Cannot call a class as a function")
}
honey.def("lib:jquery", function(e) {
    var t = e.EquipmentPlugin
      , i = e.VideoPlayerTPL
      , o = e.PLOG
      , n = e.Utils.debounce
      , s = ["94l6w226e", "94l6w226d", "94l6w226c", "94l6w226b", "94l6w226a", "94l6w2269", "94l6w2268", "94l6w226n", "94l6w226m", "95exclax3", "95exclax2", "95exclax1", "95exclax0", "95exclawz", "95exclawy", "95exclawx", "95exclaww", "95exclaxb", "95exclaxa", "95exclapz", "95exclapy", "95exclapx", "95exclapw", "95exclapv", "95exclapu", "95exclapt", "95exclaps", "95exclaq7", "95exclaq6", "95exclaiv", "90f12f8na", "90f12f8n9", "90f12f8n8", "90f12f8n7", "90f12f8n6", "90f12f8n5", "90f12f8n4", "90f12f8nj", "90f12f8ni", "90f945a4n", "90f945a4m", "90f945a4l", "90f945a4k", "90f945a4j", "90f945a4i", "90f945a4h", "90f945a4g", "90f945a4v", "90f945a4u", "90f9459xj", "90f9459xi", "90f9459xh", "90f9459xg", "90f9459xf", "90f9459xe", "90f9459xd", "90f9459xc", "90f9459xr", "90f9459xq", "90f9459qf", "90f9459qe", "90f9459qd", "90f9459qc", "90f9459qb", "90f9459qa", "90f9459q9", "90f9459q8", "90f9459qn", "90f9459qm", "90f945b47", "90f12gp6u", "90f12gp6t", "90f12gp6s", "90f12gp6r", "90f12gp6q", "90f12gp6p", "90f12gp6o", "90f12gp73", "90f12gp72", "90f8tgeiv", "90f8tgeiu", "90f8tgeit", "90f8tgeis", "90f8tgeir", "90f8tgeiq", "90f8tgeip", "90f8tgeio", "90f8tgej3", "90f8tgej2", "90f8tgebr", "90f8tgebq", "90f8tgebp", "90f8tgebo", "90f8tgebn", "90f8tgebm", "90f8tgebl", "90f8tgebk", "90f8tgebz", "90f8tgeby", "90f8tge4n", "90f8tge4m", "90f8tge4l", "90f8tge4k", "90f8tge4j", "90f8tge4i", "90f8tge4h", "90f8tge4g", "90f8tge4v", "90f8tge4u", "90f8tgfif", "90f62oqfm", "90f945b46", "90f945b45", "90f945b44", "90f945b43", "90f945b42", "90f945b41", "90f945b40", "90f945b4f", "90f945b4e", "90f945ax3", "90f945ax2", "90f945ax1", "90f945ax0", "90f945awz", "90f945awy", "90f945awx", "90f945aww", "90f945axb", "90f945axa", "90f945apz"]
      , a = {
        box: "mgtv-player-wrap",
        video: "mgtv-video-income",
        autoplay: !1,
        theme: "#b7daff",
        loop: !1,
        lang: -1 !== navigator.language.indexOf("zh") ? "zh" : "en",
        screenshot: !1,
        hotkey: !0,
        preload: "auto",
        apiBackend: ""
    }
      , r = {
        VERSION: "5.5.2",
        VERSION_IPAD: "5.5.2",
        RELEASE_DATE: "2017-11-12 18:50:11",
        API_ENDPOINT: "//pcweb.api.mgtv.com",
        DEFINITION_SD: "标清",
        DEFINITION_HD: "高清",
        DEFINITION_UD: "超清",
        DEFINITION_BD: "蓝光",
        NOOP: function() {},
        DEFAULT_VOLUME: .5,
        DEBUNCE_TIME: 200
    }
      , d = {
        DEBUG_SWITCH: 1,
        ANTI_STEALING_LINK: 1,
        DANMAKU_SWITCH: 0,
        P2P_SWITCH: 1,
        SHARE_SWITCH: 0,
        AUTOPLAY: 1,
        USE_HOT_KEY: 1,
        USE_CONTEXT_MENU: 1,
        WEB_JUMP: 0,
        ALLOW_FULL_SCREEN: 1,
        ALLOW_FLOAT_WINDOW: 1
    }
      , l = {
        GET_NEXT_VIDEO: function() {
            if (window.FLASH_MGTV_FUN && window.FLASH_MGTV_FUN.getPlayerNextId)
                return window.FLASH_MGTV_FUN.getPlayerNextId()
        },
        GET_PLAY_EVENT: function() {
            e.MGTV_VIDEO_FUN && e.MGTV_VIDEO_FUN.getPlayEvent && e.MGTV_VIDEO_FUN.getPlayEvent()
        },
        GET_STOP_EVENT: function() {
            e.MGTV_VIDEO_FUN && e.MGTV_VIDEO_FUN.getStopEvent && e.MGTV_VIDEO_FUN.getStopEvent()
        },
        GET_SEEK_EVENT: function(t, i) {
            e.MGTV_VIDEO_FUN && e.MGTV_VIDEO_FUN.getSeekEvent && e.MGTV_VIDEO_FUN.getSeekEvent(t, i)
        },
        SET_FLOATING_WINDOW: function(e) {
            window.FLASH_FLOATIONPLAYERWINDOW && window.FLASH_FLOATIONPLAYERWINDOW(e)
        },
        SET_FLOATING_WINDOW_STATUS: function(e) {
            window.clipStart && window.clipStart(e)
        },
        INIT_VIP_DIALOG: function(t) {
            return e.vipDialog ? new e.vipDialog(t) : null
        },
        SHOW_HD_DIALOG: function(e) {
            window.FLASH_MGTV_VIDEOHD && window.FLASH_MGTV_VIDEOHD(e)
        },
        GET_PAYMENT_INFO: function(e, t) {
            window.FLASH_showPayMovie && window.FLASH_showPayMovie(e, t)
        },
        GET_FLASH_MOVIEOBJECT: function(e) {
            var t = document.getElementById(e);
            return -1 != navigator.appName.indexOf("Microsoft") ? t : t.getElementsByTagName("embed").length > 0 ? t.getElementsByTagName("embed")[0] : t
        }
    }
      , u = function() {
        function u(t) {
            _classCallCheck(this, u),
            t = $.extend(a, t);
            this.playWrap = "#" + t.box;
            var o = $(this.playWrap);
            this.$box = o,
            this.$box.html(i.floatingMasker + i.videoTemplate),
            this.$video = o.find("#mgtv-video-player")[0],
            $("div[node-type=loading-box]").show(),
            this.previewType = 2,
            this.previewSelector = 1 == this.previewType ? "div[node-type=time-preview-box]" : "div[node-type=time-preview-img-box]",
            this.videoinfo = {},
            this.nextVideoInfo = null,
            d.P2P_SWITCH = t.ptp ? d.P2P_SWITCH : t.ptp,
            this.isSkip = "no" != $.LS.get("video-history-skip"),
            this.isSkiplock = !0,
            this.skipStartTime = 0,
            this.skipEndTime = 0,
            this.recordPlayTime = 0,
            this.duration = 0,
            this.volume = $.LS.get("video-history-volume") || r.DEFAULT_VOLUME,
            this.isReady = !1,
            this.hasPlayStart = !1,
            this.switching = !1,
            this.timelineUpdateTimer = null,
            this.startInfoFlag = !1,
            this.controlsTimer = null,
            this.autoHideControlsLock = !1,
            this.playInfoDomains = 0,
            this.historyDef = this.getHistoryDef(),
            this.vipDialog = l.INIT_VIP_DIALOG(this.playWrap),
            l.SET_FLOATING_WINDOW("2"),
            this.isReadyNonce = !1;
            for (var n = e.Utils.getUrlParam("cxid"), c = 0; c < s.length; c++)
                if (s[c] === n) {
                    this.nonce(),
                    this.isReadyNonce = !0;
                    break
                }
            this.init()
        }
        return _createClass(u, [{
            key: "nonce",
            value: function() {
                var e = this;
                e.initKernel(),
                e.fetchVideoInfo(),
                $(".u-icon-play").show(),
                $(".loading-box").hide(),
                e.ipadBindEvent(),
                $("body").on("click", ".u-icon-play", function() {
                    $(".u-icon-play").hide(),
                    $("#h5-show-dev-nonce").hide(),
                    e.isReadyNonce = !1,
                    e.init()
                })
            }
        }, {
            key: "init",
            value: function() {
                if (this.isReadyNonce)
                    return !1;
                t.ua.ipad ? (this.stkLog(),
                this.initFrontAdPlugin(),
                this.ipadBindEvent()) : (this.stkLog(),
                this.initKernel(),
                this.fetchVideoInfo(),
                this.createContextMenu(),
                this.initFrontAdPlugin(),
                this.pcBindEvent())
            }
        }, {
            key: "stkLog",
            value: function(e) {
                if ("undefined" == typeof STK)
                    return !1;
                var t = STK && new STK.start;
                if (e)
                    var i = e;
                else
                    i = {
                        bid: "1.1.91",
                        edu: /iPad/i.test(navigator.userAgent) ? "ipad" : "pcweb",
                        act: "webpm",
                        dom: $("#mgtv-video-player").length,
                        modl: window.VIDEOINFO.mod || "",
                        vid: window.VIDEOINFO.vid || "",
                        cid: window.VIDEOINFO.rid || "",
                        swf: "ish5",
                        ch: STK.$.getCxid("cxid") || "",
                        os: STK.$.os.sys || "",
                        suuid: STK.$.getSessionStorage("player_suuid"),
                        browser: STK.$.browser.brName + " " + STK.$.browser.version
                    };
                t.pageStart(i)
            }
        }, {
            key: "getPlayInfoConfig",
            value: function() {
                var e = window.STK && window.STK.$.getCxid("cxid") || ""
                  , t = "/player/video?did=" + (STK.$.stkuuid() || "") + "&suuid=" + (STK.$.getSessionStorage("player_suuid") || "") + ("https:" == window.location.protocol ? "&_support=10000000" : "") + "&cxid=" + e + "&"
                  , i = "/player/getSource?" + ("https:" == window.location.protocol ? "_support=10000000&" : "")
                  , o = this.historyDef ? this.historyDef.name : "";
                return {
                    enableP2P: !!d.P2P_SWITCH,
                    videoId: "mgtv-video-player",
                    debug: !1,
                    p2pDebug: !1,
                    isNativePriority: !0,
                    defaultRate: o,
                    playInfoDomains: ["//pstream.api.mgtv.com", "//pstream2.api.mgtv.com", "//pstream3.api.mgtv.com"],
                    playInfoUrl: t,
                    sourceUrl: i,
                    p2pServer: "seed-server.api.mgtv.com",
                    ver: "0.3.0301",
                    didFun: function() {
                        return STK.$.stkuuid() || ""
                    },
                    sessionidFun: function() {
                        return STK.$.sessionid() || ""
                    },
                    suuidFun: function() {
                        return STK.$.getSessionStorage("player_suuid") || ""
                    }
                }
            }
        }, {
            key: "initKernel",
            value: function() {
                var e = this.getPlayInfoConfig();
                this.kernel = window.__MGTVVIDEO = new MGTVVodVideo(e),
                this.kernel.volume = parseFloat(this.volume),
                this.version = r.VERSION + ("1" == this.kernel.p2pSwitch ? ".1" : "");
                var i = t.ua.ipad ? "imgotv-padh5-" + r.VERSION_IPAD + ("1" == this.kernel.p2pSwitch ? ".1" : "") : "imgotv-pch5-" + this.version;
                o.init({
                    version: i
                }),
                this.bindVideoEvent()
            }
        }, {
            key: "encodeTk2",
            value: function(e) {
                return btoa(e).replace(/\+/g, "_").replace(/\//g, "~").replace(/=/g, "-").split("").reverse().join("")
            }
        }, {
            key: "decodeTK2",
            value: function(e) {
                return atob(e.split("").reverse().join("").replace(/-/g, "=").replace(/~/g, "/").replace(/_/g, "+"))
            }
        }, {
            key: "parseTk2",
            value: function(e) {
                var t = {};
                return e.split("|").forEach(function(e, i, o) {
                    var n = e.split("=");
                    n && n.length && (t[n[0]] = n[1])
                }),
                t
            }
        }, {
            key: "getPlayInfo",
            value: function() {
                var e = this
                  , t = this.getPlayInfoConfig()
                  , i = t.playInfoDomains
                  , n = "did=" + t.didFun() + "|pno=1121|ver=0.3.1211|clit=" + Math.floor((new Date).getTime() / 1e3)
                  , s = window.VIDEOINFO.vid || ""
                  , a = i[e.playInfoDomains] + t.playInfoUrl + "tk2=" + this.encodeTk2(n) + "&video_id=" + s + "&type=pad"
                  , r = {
                    api: "playinfo",
                    url: a
                }
                  , d = $.ajax({
                    url: a,
                    timeout: 4e3,
                    type: "get",
                    dataType: "jsonp",
                    success: function(t) {
                        e.playInfoCenter(t, r)
                    },
                    complete: function(t, n) {
                        "timeout" == n && (d.abort(),
                        e.playInfoDomains++,
                        e.playInfoDomains < i.length && e.getPlayInfo(),
                        o.requestError({
                            type: o.RequestType.CMS,
                            h: r.url
                        }))
                    }
                })
            }
        }, {
            key: "playInfoCenter",
            value: function(t, i) {
                var n = this;
                if (t && 200 == t.code && t.data) {
                    var s = t.data;
                    if (n.videoinfo = s,
                    n.tk2Object = n.parseTk2(n.decodeTK2(s.atc.tk2)),
                    n.isReadyNonce)
                        return $("#h5-show-dev-nonce").attr("src", n.videoinfo.info.thumb).show(),
                        !1;
                    VIDEOINFO.user = s.user,
                    VIDEOINFO.info = s.info,
                    e.INCOME_VIDEO.fetchAd({
                        type: "front"
                    }, s.atc, function(e) {
                        n.videoSource = e,
                        e.v.ets && (n.videoinfo.atc = e.v.ets.atc)
                    }),
                    o.requestSuccess({
                        type: o.RequestType.CMS,
                        h: i.url,
                        data: s
                    }),
                    l.GET_PAYMENT_INFO(s.user, s.info)
                } else
                    t && 200 != t.code && n.showError({
                        code: t.code,
                        msg: t.msg
                    }),
                    o.requestError({
                        type: o.RequestType.CMS,
                        h: i.url
                    })
            }
        }, {
            key: "fetchVideoInfo",
            value: function() {
                var t = this;
                t.kernel.getPlayInfo(window.VIDEOINFO.vid, function(i, n) {
                    if (t.videoinfo = i,
                    t.tk2Object = t.parseTk2(t.decodeTK2(i.atc.tk2)),
                    t.isReadyNonce)
                        return $("#h5-show-dev-nonce").attr("src", t.videoinfo.info.thumb).show(),
                        !1;
                    VIDEOINFO.user = i.user,
                    VIDEOINFO.info = i.info,
                    e.INCOME_VIDEO.ading || t.startInfoFlag ? e.INCOME_VIDEO.fetchAd({
                        type: "front"
                    }, i.atc, function(e) {
                        e.v.ets ? e.v.data ? (t.kernel.getSource(VIDEOINFO.vid, e.v.ets.atc, null, function(i) {
                            t.videoinfo.stream = i.stream,
                            e.v.data || t.startInfo(i)
                        }),
                        t.videoinfo.atc = e.v.ets.atc) : t.videoinfo.atc = e.v.ets.atc : t.kernel.getSource(VIDEOINFO.vid, i.atc, t.videoinfo, function(e) {
                            t.videoinfo.stream = e.stream
                        })
                    }) : t.kernel.getSource(VIDEOINFO.vid, i.atc, t.videoinfo, function(e) {
                        t.videoinfo.stream = e.stream,
                        t.startInfo(e)
                    }),
                    o.requestSuccess({
                        type: o.RequestType.CMS,
                        h: n.url,
                        data: i
                    }),
                    l.GET_PAYMENT_INFO(i.user, i.info)
                }, function(e, i) {
                    e && 200 != e.code && t.showError({
                        code: e.code,
                        msg: e.msg
                    })
                })
            }
        }, {
            key: "initFrontAdPlugin",
            value: function() {
                e.INCOME_VIDEO && (o.playAdStart(),
                $(document).on("player.adend", this.adEndHandler.bind(this)),
                e.INCOME_VIDEO.AdPlayer({
                    vip: 1 == e.Utils.getCookie("vipStatus") ? 1 : 0,
                    passport: "",
                    boxId: "#mgtv-video-wrap"
                }))
            }
        }, {
            key: "adEndHandler",
            value: function(i, n) {
                var s = this;
                if (t.ua.ipad && s.initKernel(),
                o.playAdEnd(n.adId, n.adType, n.adTime, n.adNum),
                "fail api" == n.endType && 0 == e.Utils.getCookie("vipStatus")) {
                    $("div[node-type=loading-box]").hide(),
                    $("[v-mvp-ad=player_ad_error]").show();
                    var a = $("[v-mvp-ad=player_ad_error_count]")
                      , r = s.tk2Object.blt || 60;
                    a.html(r);
                    var d = setInterval(function() {
                        var e = parseInt(a.html());
                        e <= 0 ? clearInterval(d) : a.html(e - 1)
                    }, 1e3);
                    setTimeout(function() {
                        $("[v-mvp-ad=player_ad_error]").hide(),
                        $("#mgtv-video-player").show(),
                        s.kernel.getSource(VIDEOINFO.vid, s.videoinfo.atc, s.videoinfo, function(e) {
                            s.videoinfo.stream = e.stream,
                            s.startInfo(s.videoinfo),
                            s.kernel.playVideo()
                        })
                    }, 1e3 * r - 100)
                } else
                    $(".mgtv-video-income").hide(),
                    $("#mgtv-video-player").show(),
                    s.videoinfo.info && s.kernel.streamDomain ? (s.startInfo(s.videoinfo),
                    !s.isReady && s.kernel && s.kernel.playVideo()) : s.kernel.getSource(VIDEOINFO.vid, s.videoinfo.atc, s.videoinfo, function(e) {
                        s.videoinfo.stream = e.stream,
                        s.startInfo(s.videoinfo),
                        !s.isReady && s.kernel && s.kernel.playVideo()
                    })
            }
        }, {
            key: "preLoadFrames",
            value: function() {
                var e = this.videoinfo.frame;
                e && e.images && e.images.length && e.images.forEach(function(e) {
                    (new Image).src = e
                })
            }
        }, {
            key: "initDanmuku",
            value: function(t) {
                var i = this.$box.find(".danmu-box");
                this.danmaku = new e.Danmaku,
                this.danmaku.init({
                    container: i[0],
                    videoinfo: this.videoinfo,
                    video: this.$video,
                    comments: [],
                    engine: "dom",
                    visible: !1,
                    speed: 100
                }),
                console.log("初始化弹幕组件，插件版本为:" + this.danmaku.version)
            }
        }, {
            key: "initFlashPlayer",
            value: function() {
                var e = {
                    statistics_bigdata_bid: 1,
                    web_jump: 3 == VIDEOINFO.rid ? 1 : "",
                    cpn: VIDEOINFO.cpn || "",
                    cid: VIDEOINFO.cid || "",
                    plid: VIDEOINFO.plid || "",
                    video_bdid: VIDEOINFO.bdid || ""
                };
                $.contextMenu.closed(),
                (new MVP.Player).create({
                    vid: VIDEOINFO.vid,
                    modId: "mgtv-player-wrap",
                    vodFlashUrl: "//player.mgtv.com/mgtv_v6_main/main.swf",
                    flashVars: e
                }),
                l.SET_FLOATING_WINDOW("1"),
                setTimeout(function() {
                    var e = l.GET_NEXT_VIDEO();
                    try {
                        l.GET_FLASH_MOVIEOBJECT("hunantv-player-1").OnGetNextVideoInfo(e)
                    } catch (t) {
                        console.log("调用FLASH失败")
                    }
                }, 2e3)
            }
        }, {
            key: "showDebugInfo",
            value: function() {
                var e = this
                  , t = this.$box.find('[data-role="txp-ui-console"]');
                if (this.debugtimer)
                    return clearInterval(this.debugtimer),
                    this.debugtimer = null,
                    void t.hide();
                var i = {
                    vid: window.VIDEOINFO.vid || this.videoinfo.info.video_id,
                    playertype: this.kernel && this.kernel.mode || "",
                    ratio: window.screen.width + "X" + window.screen.height,
                    videoSize: this.$box.width() + "X" + this.$box.height(),
                    protocol: window.location.protocol,
                    version: this.version,
                    volume: this.volume,
                    fps: "",
                    flowid: STK.$.getSessionStorage("player_suuid") || ""
                };
                for (var o in i)
                    t.find('[data-role="txp-ui-console-' + o + '"]').text(i[o]);
                t.show(),
                this.debugtimer = setInterval(function() {
                    t.find('[data-role="txp-ui-console-fps"]').text(e.danmaku.fps)
                }, 400)
            }
        }, {
            key: "showError",
            value: function(t) {
                var o = !(arguments.length > 1 && arguments[1] !== undefined) || arguments[1]
                  , n = this.$box.find(".alert-box")
                  , s = e.Utils.replaceHelper(i.errorTPL, t);
                o ? n.html(s).show() : n.hide()
            }
        }, {
            key: "createContextMenu",
            value: function(t) {
                var i = this
                  , o = [[{
                    text: "复制播放地址",
                    func: function() {
                        e.Utils.copyToClipboard(window.location.href)
                    }
                }], [{
                    text: "播放器版本: " + this.version,
                    func: function() {}
                }], [{
                    text: "内核版本: " + this.kernel.version,
                    func: function() {}
                }], [{
                    text: "使用FLASH播放器",
                    func: function() {
                        i.initFlashPlayer()
                    }
                }]];
                $(this.playWrap).contextMenu(o, {
                    name: "video",
                    obj: this.playWrap,
                    beforeShow: function() {},
                    afterShow: function() {}
                })
            }
        }, {
            key: "fetchNextVideoInfo",
            value: function(t) {
                var o = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ""
                  , n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ""
                  , s = this
                  , a = this.$box;
                if (t.constructor == Object) {
                    s.nextVideoInfo = t;
                    var r = e.Utils.replaceHelper(i.nextTPL, s.nextVideoInfo)
                      , d = e.Utils.replaceHelper(i.nextLoadTPL, s.nextVideoInfo);
                    a.find("li[node-type=next-show-box]").html(r).show(),
                    a.find("div[node-type=next-loading-box]").html(d)
                } else {
                    var l = void 0
                      , u = !1;
                    t && t.indexOf("&") ? (l = t.split("&")[0],
                    u = "false" == t.split("&")[1]) : l = t,
                    u && (o = VIDEOINFO.cid || "",
                    n = VIDEOINFO.plid || "");
                    var c = "//pcweb.api.mgtv.com/player/vinfo?video_id=" + l + "&cid=" + o + "&pid=" + n + "&cxid=" + (window.STK && window.STK.$.getCxid("cxid") || "");
                    e.Utils.ajax(c, function(t) {
                        if (t && 200 == t.code && t.data) {
                            s.nextVideoInfo = t.data,
                            s.nextVideoInfo.time = e.Utils.timeFormat(s.nextVideoInfo.duration);
                            var o = e.Utils.replaceHelper(i.nextTPL, s.nextVideoInfo)
                              , n = e.Utils.replaceHelper(i.nextLoadTPL, s.nextVideoInfo);
                            a.find("li[node-type=next-show-box]").html(o).show(),
                            a.find("div[node-type=next-loading-box]").html(n)
                        }
                    })
                }
            }
        }, {
            key: "startInfo",
            value: function(e) {
                var t = this;
                if (this.startInfoFlag = !0,
                200 != e.user.purview && 0 == e.info.trialtime)
                    return $("div[node-type=loading-box]").hide(),
                    void this.showVipDialog();
                this.preLoadFrames(),
                this.setTimelineUI(),
                this.initDanmuku(),
                this.setStartTime(),
                this.showCornerTips(this.videoinfo),
                this.setConfigUI(),
                this.setVolumeUI(),
                this.setDefinitionUI(this.kernel.curRate, this.videoinfo.stream),
                setTimeout(function() {
                    var e = l.GET_NEXT_VIDEO();
                    e && t.fetchNextVideoInfo(e)
                }, 2500),
                this.bindEvent()
            }
        }, {
            key: "setStartTime",
            value: function() {
                for (var t, o = e.Utils.getUrlParam("start_time"), n = this.videoinfo.info.watchTime, s = $.LS.get("honey-rec") ? JSON.parse($.LS.get("honey-rec")) : [], a = 0; a < s.length; a++)
                    if (VIDEOINFO.vid == s[a].vid) {
                        this.recordPlayTime = s[a].time;
                        break
                    }
                t = o || n || this.recordPlayTime,
                this.isSkip && this.skipStartTime > 0 && t < this.skipStartTime ? (this.showPlayTips(i.skipStartTPL),
                this.kernel.loadVideo(this.skipStartTime)) : (t > 0 && this.showPlayTips(e.Utils.recordTimeTPL(t)),
                this.kernel.loadVideo(t))
            }
        }, {
            key: "createPoint",
            value: function(e, t) {
                var i, o, n = void 0;
                n = this.$box.find(".u-control-loading-box"),
                i = Math.floor(e / this.duration * 100),
                o = $('<div class="point" data-title=' + t + " data-time=" + e + ' style="left:' + i + '%">'),
                n.append(o)
            }
        }, {
            key: "setTimelineUI",
            value: function() {
                var e = this;
                e.skipStartTime = Number(this.videoinfo.points.start.split("|")[0]) || 0,
                e.skipEndTime = Number(this.videoinfo.points.end.split("|")[0]) || 0,
                e.duration = this.videoinfo.info.duration;
                this.videoinfo.info.watchTime;
                e.skipStartTime > 0 && e.skipStartTime < this.duration && e.createPoint(e.skipStartTime),
                e.skipEndTime > 0 && e.skipEndTime < this.duration && e.createPoint(e.skipEndTime),
                e.points = this.videoinfo.points.content.map(function(t) {
                    var i = t.split("|");
                    return e.createPoint(i[0], i[1]),
                    {
                        time: i[0],
                        title: i[1]
                    }
                })
            }
        }, {
            key: "showCornerTips",
            value: function(e) {
                var t = e.info.paymark;
                if (200 != e.user.purview)
                    switch (t) {
                    case "1":
                        this.showPlayTips(i.vipTPL, !1, Math.floor(e.info.trialtime / 60));
                        break;
                    case "2":
                        this.showPlayTips(i.buyMovie, !1, Math.floor(e.info.trialtime / 60));
                        break;
                    case "3":
                        this.showPlayTips(i.movieTicket, !1, Math.floor(e.info.trialtime / 60))
                    }
            }
        }, {
            key: "setConfigUI",
            value: function() {
                var e = this.$box.find("div[node-type=video-set-box]");
                this.isSkip ? (e.find(".select").removeClass("focus"),
                e.find("[data=yes]").addClass("focus")) : (e.find(".select").removeClass("focus"),
                e.find("[data=no]").addClass("focus"))
            }
        }, {
            key: "setVolumeUI",
            value: function() {
                var e = 100 * this.volume;
                e <= 0 ? (this.$box.find(".volume-bar").css({
                    width: "7px"
                }),
                this.$box.find(".volume-icon").addClass("btn-no")) : this.$box.find(".u-control-voice .volume-bar").css({
                    width: e + "%"
                })
            }
        }, {
            key: "setDefinitionUI",
            value: function(e, t) {
                if (this.$box.find(".u-control-clarity .btn").html(e),
                t) {
                    var i = ""
                      , n = "";
                    t.forEach(function(t, o) {
                        n = t.name == e ? "focus " : " ",
                        n += "蓝光" == t.name ? "vip " : " ",
                        i = '<a class="' + n + '" href="javascript:;" s-data="' + o + '">' + t.name + '<svg class="icon pad-vip"><use xlink:href="#pad-vip"></use></svg></a>' + i
                    }),
                    this.$box.find("div[node-type=clarity-list]").html(i)
                }
                o.updateDef(e)
            }
        }, {
            key: "showPlayTip",
            value: function(e) {
                var t = this;
                this.$pauseTips = this.$box.find("div[action-type=v-h5-big-pause]"),
                this.$playTips = this.$box.find("div[action-type=v-h5-big-play]"),
                "play" == e && (this.$box.find(".v-h5-play").removeClass("stop").addClass("play").find("a").attr("title", "暂停"),
                this.$playTips.hide().fadeIn(),
                setTimeout(function() {
                    t.$playTips.fadeOut()
                }, 300)),
                "pause" == e && (this.$box.find(".v-h5-play").removeClass("play").addClass("stop").find("a").attr("title", "播放"),
                this.$pauseTips.hide().fadeIn(),
                setTimeout(function() {
                    t.$pauseTips.fadeOut()
                }, 300))
            }
        }, {
            key: "setPlayStatusUI",
            value: function() {
                switch (this.kernel.state) {
                case 1:
                    this.showPlayTip("pause"),
                    this.kernel.pauseVideo(),
                    this.stopUpdateTimeLineUI(),
                    d.DANMAKU_SWITCH && this.danmaku.hide();
                    break;
                case 2:
                    this.showPlayTip("play"),
                    this.kernel.playVideo(),
                    this.updateTimeLineUI(),
                    d.DANMAKU_SWITCH && this.danmaku.show();
                    break;
                case -1:
                    this.$box.find("div[action-type=v-h5-big-replay]").hide(),
                    this.kernel.replayVideo(),
                    this.updateTimeLineUI(),
                    d.DANMAKU_SWITCH && this.danmaku.show();
                    break;
                default:
                    console.log("播放状态出了问题")
                }
            }
        }, {
            key: "updateTimeLineUI",
            value: function() {
                var t = this
                  , n = 0
                  , s = this.$box
                  , a = t.duration;
                t.stopUpdateTimeLineUI(),
                t.timelineUpdateTimer = setInterval(function() {
                    if (!t.isDraggingTimeline) {
                        var r = s.find(".u-control-loading-box").width()
                          , d = s.find(".progress-bar")
                          , u = s.find(".progress-button")
                          , c = (Math.round(d.width() / r * a),
                        t.kernel.curTime)
                          , f = c / a * r;
                        s.find(".ctime").html(e.Utils.timeFormat(c)),
                        d.css({
                            width: f + "px"
                        }),
                        u.css({
                            left: 100 * (f - u.width() / 2) / r + "%"
                        });
                        var h = t.kernel.duration;
                        t.isSkip && t.skipEndTime > 0 && (h = t.skipEndTime);
                        var p = t.kernel.bufferTime / a;
                        s.find(".buffered").css({
                            width: 100 * p + "%"
                        });
                        var v = Math.floor(h) - Math.floor(c);
                        if (o.updatePlayTime(c),
                        ++n % 100 == 0 && (n = 0,
                        l.GET_SEEK_EVENT(Math.ceil(c), a)),
                        200 != t.videoinfo.user.purview && c - t.videoinfo.info.trialtime > 0)
                            return t.showVipDialog(),
                            void t.stopUpdateTimeLineUI();
                        t.isSkip && t.skipEndTime > 0 && v <= 10 && t.isSkiplock && (t.showPlayTips(i.skipEndTPL, !0),
                        t.isSkiplock = !1),
                        t.nextVideoInfo && v <= 10 && h > 0 && (v = v > 0 ? v : 0,
                        t.showNextPlayDialog(Boolean(v), v),
                        0 == v && t.endVideo())
                    }
                }, 512)
            }
        }, {
            key: "stopUpdateTimeLineUI",
            value: function() {
                this.timelineUpdateTimer && clearInterval(this.timelineUpdateTimer),
                this.timelineUpdateTimer = null
            }
        }, {
            key: "autoHideControls",
            value: function() {
                var e = this;
                this.controlsTimer && clearTimeout(this.controlsTimer),
                this.controlsTimer = setTimeout(function() {
                    e.autoHideControlsLock ? e.autoHideControls() : (e.$box.find(".u-clarity-box").hide(),
                    e.$box.find(".m-player-h5").removeClass("controls-show").addClass("controls-hide"))
                }, 3e3)
            }
        }, {
            key: "setDanmuVisible",
            value: function(e) {
                console.log("设置弹幕可见: " + e),
                e ? this.danmaku.show() : this.danmaku.hide()
            }
        }, {
            key: "get_time",
            value: function() {
                return this.kernel.curTime
            }
        }, {
            key: "sendDanmu",
            value: function(e) {
                console.log("发送弹幕：" + e),
                this.danmaku.emit({
                    content: e
                })
            }
        }, {
            key: "exitFullScreen",
            value: function() {
                this.$box.get(0);
                $("div[node-type=video-wrapper]").removeAttr("style"),
                $("#mgtv-video-player").attr("controls", !1),
                fullScreenApi.cancelFullScreen(),
                $("body").off("touchmove.ipad"),
                this.onResize(),
                e.INCOME_VIDEO && (e.INCOME_VIDEO.windowStaus = 0)
            }
        }, {
            key: "enterFullScreen",
            value: function() {
                var i = this
                  , o = this.$box.get(0);
                if (t.ua.ipad) {
                    var n = document.getElementById("mgtv-video-player");
                    n.webkitEnterFullscreen(),
                    n.addEventListener("webkitendfullscreen", function() {
                        i.kernel.playVideo()
                    })
                } else
                    fullScreenApi.requestFullScreen(o),
                    $("li[action-type=v-video-fullscreen] a").addClass("btn-no");
                this.onResize(),
                e.INCOME_VIDEO && (e.INCOME_VIDEO.windowStaus = 1)
            }
        }, {
            key: "onResize",
            value: function() {
                d.DANMAKU_SWITCH && this.danmaku.resize()
            }
        }, {
            key: "bindEvent",
            value: function() {
                if (!this.hasBindEvent) {
                    this.hasBindEvent = !0;
                    var s = this
                      , a = s.$box;
                    a.on("click", "li[action-type=v-video-fullscreen] a", function(e) {
                        e.preventDefault(),
                        fullScreenApi.isFullScreen() ? (s.exitFullScreen(),
                        $(this).removeClass("btn-no")) : s.enterFullScreen()
                    }),
                    a.on("mousedown touchstart", ".u-control-loading-box", function() {
                        s.stopUpdateTimeLineUI(),
                        document.addEventListener("mousemove", r),
                        document.addEventListener("mouseup", d),
                        s.autoHideControlsLock = !0
                    }),
                    a.on("touchmove", ".progress", function(e) {
                        r(e)
                    }),
                    a.on("touchend touchcancel", ".progress", function(e) {
                        d(e)
                    }),
                    a.on("mousemove", ".progress", function(i) {
                        if (a.hasClass("floating"))
                            return !1;
                        var o = a.find(".u-control-loading-box").width()
                          , n = a.find(s.previewSelector).width()
                          , r = ("mousemove" == i.type ? i.pageX : i.originalEvent.changedTouches[0].pageX) - a.find(".progress").offset().left - 10
                          , d = (r = r < 0 ? 0 : r) / o * s.duration
                          , l = a.find(".progress-indicator").width()
                          , u = s.getFrameImage(s.videoinfo.frame, d);
                        if (!u || t.ua.ipad)
                            return a.find(s.previewSelector).hide(),
                            void a.find(".progress-indicator").hide();
                        if (r <= 0)
                            a.find(".progress-indicator").css({
                                left: "0px"
                            });
                        else if (o >= r) {
                            var c = Math.min(Math.max(0, r - n / 2), o - n);
                            a.find(s.previewSelector).css({
                                left: c + "px"
                            }).find(".pic").css({
                                backgroundImage: "url(" + u.image + ")",
                                backgroundPosition: "-" + u.x + "px -" + u.y + "px"
                            }),
                            a.find(s.previewSelector).find(".arr").css({
                                left: r - c
                            }),
                            a.find(".progress-indicator").css({
                                left: r - l / 2 + "px"
                            })
                        }
                        a.find("[node-type=time-preview]").html(e.Utils.timeFormat(d))
                    });
                    var r = function(t) {
                        var i = t || window.event
                          , o = 0;
                        o = "mousemove" == i.type ? i.clientX : i.originalEvent.changedTouches[0].clientX;
                        var n, r = a.find(".u-control-loading-box"), d = r.width(), l = o - e.Utils.getElementViewLeft(r[0]);
                        n = (l = Math.min(Math.max(0, l), d)) / d,
                        s.isDraggingTimeline = !0,
                        a.find(".progress-bar").css("width", 100 * n + "%"),
                        a.find(".progress-button").css({
                            left: 100 * (l - $(".progress-button").width() / 2) / d + "%"
                        }),
                        s.autoHideControlsLock = !0
                    }
                      , d = function v(e) {
                        var t = e || window.event;
                        s.isDraggingTimeline = !1;
                        var i = a.find(".u-control-loading-box").width();
                        s.stopUpdateTimeLineUI(),
                        a.find(".buffered").css({
                            left: "0%",
                            width: "0%"
                        });
                        var n = 0;
                        n = "mouseup" == t.type ? t.pageX : t.originalEvent.changedTouches[0].pageX,
                        s.kernel.state;
                        var d = (n - a.find(".progress").offset().left - 10) / i * s.duration;
                        if (d = Math.min(Math.max(0, d), s.duration),
                        200 != s.videoinfo.user.purview && d - s.videoinfo.info.trialtime > 0)
                            return s.showVipDialog(),
                            void s.stopUpdateTimeLineUI();
                        o.playDrag({
                            seektime: d,
                            isCurPoint: 0,
                            dragType: 1
                        }),
                        s.kernel.seekVideo(d),
                        l.GET_SEEK_EVENT(Math.ceil(d), s.duration),
                        s.updateTimeLineUI(),
                        a.find("div[node-type=next-loading-box]").hide(),
                        s.showLoading(),
                        document.removeEventListener("mouseup", v),
                        document.removeEventListener("mousemove", r),
                        s.autoHideControlsLock = !1,
                        s.autoHideControls(),
                        s.$box.find("div[action-type=v-h5-big-replay]").hide(),
                        s.$box.find("li[action-type=v-h5-player]").removeClass("stop").addClass("play")
                    }
                      , u = s.volume
                      , c = function(t) {
                        var i = t || window.event
                          , o = $(".volume-bar-holder")[0]
                          , n = (i.clientX - e.Utils.getElementViewLeft(o)) / a.find(".volume-bar-holder").width();
                        n = (n = n > 0 ? n : 0) < 1 ? n : 1,
                        a.find(".volume-bar").css({
                            width: 100 * n + "%"
                        }),
                        n > 0 ? a.find(".volume-icon").removeClass("btn-no") : a.find(".volume-icon").addClass("btn-no"),
                        s.kernel.volume = n,
                        s.volume = n,
                        $.LS.set("video-history-volume", n)
                    }
                      , f = function m() {
                        document.removeEventListener("mouseup", m),
                        document.removeEventListener("mousemove", c)
                    };
                    $(".volume-bar-holder").on("mousedown", function(e) {
                        document.addEventListener("mousemove", c),
                        document.addEventListener("mouseup", f)
                    }),
                    $(".volume-bar-holder").on("mouseup", function(e) {}),
                    a.on("click touchstart", ".u-clarity-box a", function(t) {
                        t.preventDefault();
                        var n = $(this)
                          , a = n.text()
                          , r = n.attr("s-data")
                          , d = e.Utils.getCookie("vipStatus");
                        if (3 == r && 1 != d)
                            s.setPlayStatusUI("stop"),
                            l.SHOW_HD_DIALOG("开通会员");
                        else {
                            1 !== s.kernel.state && s.showPlayTip("play"),
                            n.addClass("focus").siblings().removeClass("focus"),
                            s.$box.find(".u-control-clarity .btn").html(a),
                            $.LS.set("video-history-definition", JSON.stringify({
                                name: a,
                                type: r
                            })),
                            o.playSwitch(s.kernel.bitrates[r]),
                            s.switching = !0,
                            s.kernel.switchVideo(r),
                            this.hasPlayStart = !1;
                            var u = e.Utils.replaceHelper(i.bitRate, {
                                text: a
                            });
                            s.showPlayTips(u)
                        }
                    }),
                    a.on("mouseenter", ".progress", function(e) {
                        a.find(s.previewSelector).show(),
                        a.find(".progress-indicator").show()
                    }),
                    a.on("mouseleave", ".progress", function(e) {
                        a.find(s.previewSelector).hide(),
                        a.find(".progress-indicator").hide()
                    }),
                    a.on("click touchstart", "div[node-type=video-set-box] .select", function(e) {
                        var t = $(this);
                        t.siblings().removeClass("focus"),
                        t.addClass("focus");
                        var i = t.attr("data") || "yes";
                        $.LS.set("video-history-skip", i)
                    }),
                    a.on("click", 'a[action-type="next"]', function(e) {
                        return e.preventDefault(),
                        s.nextPlayFun(),
                        !1
                    }),
                    a.on("click", "li[node-type=video-setting]", function(e) {
                        var t = $(this).find(".u-set-box");
                        t.is(":hidden") ? t.show() : t.hide()
                    });
                    var h = null;
                    a.on("click", "[action-type=v-ui-set]", function(e) {
                        $("[node-type=video-set-box]").show(),
                        clearTimeout(h),
                        h = setTimeout(function() {
                            $("[node-type=video-set-box]").hide()
                        }, 5e3)
                    }),
                    a.on("mouseenter", "[node-type=video-set-box]", function(e) {
                        clearTimeout(h)
                    }),
                    a.on("mouseleave", "li[node-type=video-setting]", function(e) {
                        e.preventDefault(),
                        $(this).find(".u-set-box").hide()
                    }),
                    a.on("mouseleave", ".u-control-clarity", function(e) {
                        e.preventDefault(),
                        $(this).find(".u-clarity-box").hide()
                    }),
                    a.on("click touchstart", ".u-control-clarity", function(e) {
                        e.preventDefault();
                        var t = $(this).find(".u-clarity-box");
                        t.is(":hidden") ? t.show() : t.hide()
                    }),
                    a.on("mousedown", ".volume-bar-holder", function(t) {
                        var i = $(".volume-bar-holder")[0]
                          , o = (t.clientX - e.Utils.getElementViewLeft(i)) / a.find(".volume-bar-holder").width();
                        (o = (o = o > 0 ? o : 0) < 1 ? o : 1) > 0 ? a.find(".volume-icon").removeClass("btn-no") : a.find(".volume-icon").addClass("btn-no");
                        var n = t.pageX - $(this).offset().left;
                        if (n < 0 || n > $(this).width())
                            return !1;
                        a.find(".volume-bar").css({
                            width: 100 * o + "%"
                        });
                        var r = a.find(".volume-bar").width() / $(this).width();
                        s.kernel.volume = r,
                        s.volume = r,
                        $.LS.set("video-history-volume", r)
                    }),
                    a.on("mousedown", ".volume-icon", function(e) {
                        e.preventDefault(),
                        $(this).hasClass("btn-no") ? (u = s.kernel.volume = s.volume,
                        a.find(".volume-bar").css({
                            width: 100 * u + "%"
                        }),
                        $(this).removeClass("btn-no")) : (s.volume = s.kernel.volume,
                        u = s.kernel.volume = 0,
                        a.find(".volume-bar").css({
                            width: "7px"
                        }),
                        $(this).addClass("btn-no")),
                        $.LS.set("video-history-volume", u)
                    }),
                    a.on("click", "li[action-type=v-h5-player], .u-video, [action-type=v-h5-big-replay]", n(200, function(e) {
                        t.ua.ipad || s.setPlayStatusUI()
                    })),
                    a.on("touchstart", "li[action-type=v-h5-player]", n(200, function(e) {
                        s.setPlayStatusUI()
                    })),
                    a.on("mousemove", "#mgtv-video-wrap", function(e) {
                        s.$box.hasClass("floating") || a.find(".m-player-h5").removeClass("controls-hide").addClass("controls-show"),
                        s.autoHideControls()
                    }),
                    a.on("touchend", "#video-wrapper-box", function(e) {
                        if (a.find(".m-player-h5").hasClass("controls-show"))
                            return s.controlsTimer && clearTimeout(s.controlsTimer),
                            !s.autoHideControlsLock && (s.$box.find(".u-clarity-box").hide(),
                            s.$box.find(".m-player-h5").removeClass("controls-show").addClass("controls-hide"),
                            !1);
                        s.$box.hasClass("floating") || a.find(".m-player-h5").removeClass("controls-hide").addClass("controls-show"),
                        s.autoHideControls()
                    }),
                    a.on("mouseleave", "#mgtv-video-wrap", function(e) {
                        if (s.controlsTimer && clearTimeout(s.controlsTimer),
                        s.autoHideControlsLock)
                            return !1;
                        s.$box.find(".u-clarity-box").hide(),
                        s.$box.find(".m-player-h5").removeClass("controls-show").addClass("controls-hide")
                    }),
                    a.on("click", "[action-type=video-error-box]", function(e) {
                        s.showError({}, !1)
                    });
                    var p = $(document);
                    p.on("play.video.reload", function(e, t) {
                        s.reset(),
                        p.trigger("page.reload.mock", t)
                    }),
                    $(document).on("keyup", function(e) {
                        var t = e.which || e.keyCode;
                        if (document.activeElement && "body" == document.activeElement.tagName.toLowerCase())
                            switch (t) {
                            case 38:
                                console.log("up pressed");
                                break;
                            case 40:
                                console.log("down pressed");
                                break;
                            case 37:
                                console.log("left pressed");
                                break;
                            case 39:
                                console.log("right pressed");
                                break;
                            case 13:
                                console.log("enter pressed");
                                break;
                            case 339:
                                break;
                            case 27:
                                var i = document.fullscreenEnabled || window.fullScreen || document.webkitIsFullScreen || document.msFullscreenEnabled;
                                console.log(i),
                                $("li[action-type=v-video-fullscreen] a").removeClass("btn-no")
                            }
                    }),
                    $(window).on("onResizeEx", function(e) {
                        s.onResize()
                    }),
                    document.addEventListener("webkitfullscreenchange", function(e) {
                        e.currentTarget.webkitIsFullScreen || $("li[action-type=v-video-fullscreen] a").removeClass("btn-no")
                    }),
                    a.on("click touchstart", "a[action-type=v-ui-tips], a[action-type=v-ui-coupons], a[action-type=v-ui-buy]", function(e) {
                        s.showVipDialog()
                    }),
                    this.$video.addEventListener("timeupdate", function(e) {
                        s.danmaku.timeupdate(s.$video.currentTime)
                    }),
                    this.$video.addEventListener("error", function(e) {
                        console.log("<VIDEO> 视频发生错误: " + e)
                    }),
                    this.$video.addEventListener("playStart", function(t) {
                        "playStart" != t.type || e.INCOME_VIDEO.ading || (s.isReady = !0,
                        s.kernel.playVideo(),
                        s.updateTimeLineUI())
                    }),
                    this.$video.addEventListener("onMetadata", function(t) {
                        a.find(".ctime").html(e.Utils.timeFormat(0)),
                        a.find(".ttime").html(e.Utils.timeFormat(s.duration))
                    }),
                    this.$video.addEventListener("seekNotify", function(e) {
                        s.showLoading(!1),
                        s.danmaku.seek(s.$video.currentTime)
                    })
                }
            }
        }, {
            key: "bindVideoEvent",
            value: function() {
                this.$video.addEventListener("playStart", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("playStop", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("getDispatcher", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("playinfo", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("log", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("playing", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("pauseNotify", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("unpauseNotify", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("bufferEmpty", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("bufferFull", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("seekNotify", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("streamError", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("hlsSegmentLoaded", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("getSource", this.videoEventHandler.bind(this)),
                this.$video.addEventListener("waiting", function(e) {
                    console.log("waiting")
                })
            }
        }, {
            key: "pcBindEvent",
            value: function() {
                var e = this;
                e.$box.on("mouseenter", "a[action-type=next]", function() {
                    e.$box.find("li[node-type=next-show-box]").find('[node-type="next-box"]').show()
                }),
                e.$box.on("mouseleave", "a[action-type=next]", function() {
                    e.$box.find("li[node-type=next-show-box]").find('[node-type="next-box"]').hide()
                })
            }
        }, {
            key: "ipadBindEvent",
            value: function() {
                var t = this
                  , i = $(this).data("pos")
                  , o = "https://mtest.api.hunantv.com/download.html?from=pcweb&start_time=" + (t.$video.currentTime || 0) + "&clipId=" + (VIDEOINFO.cid || "") + "&plId=" + (VIDEOINFO.bdid || "") + "&videoId=" + (VIDEOINFO.vid || "") + "&type=1&pos=pc" + i;
                $(document).on("touchstart", "[node-type=guide-app]", function(e) {
                    $("a[node-type=guide-app]").attr("href", o),
                    t.stkLog({
                        bid: "4.1.2",
                        act: "click",
                        pos: i
                    })
                }),
                $("#ipad-play-Btn").on("touchstart", function() {
                    $("#video_ipad_box").hide(),
                    t.getPlayInfo(),
                    e.INCOME_VIDEO.initIpad()
                })
            }
        }, {
            key: "showPayBox",
            value: function(e, t) {
                window.FLASH_showPayMovie && window.FLASH_showPayMovie(e, t)
            }
        }, {
            key: "showEndVideo",
            value: function() {
                this.nextVideoInfo || this.$box.find("div[action-type=v-h5-big-replay]").show()
            }
        }, {
            key: "showVipDialog",
            value: function() {
                this.vipDialog && ($("div[node-type=loading-box]").hide(),
                this.$box.html(""),
                this.vipDialog.open(this.videoinfo),
                this.$box.hasClass("floating") && (this.$box.removeClass("floating"),
                this.$box.css({
                    right: "0",
                    top: "0"
                }),
                l.SET_FLOATING_WINDOW_STATUS(0)))
            }
        }, {
            key: "showNextPlayDialog",
            value: function(e, t) {
                e ? (this.$box.find("div[node-type=next-loading-box]").show(),
                this.$box.find("i[node-type=next-time-countdown]").html(t)) : this.$box.find("div[node-type=next-loading-box]").hide()
            }
        }, {
            key: "endVideo",
            value: function() {
                if (e.INCOME_VIDEO.padading)
                    return !1;
                this.stopUpdateTimeLineUI(),
                this.showLoading(!1),
                this.showEndVideo(),
                o.playStop(),
                l.GET_STOP_EVENT(),
                setTimeout(this.nextPlayFun.bind(this), 500)
            }
        }, {
            key: "nextPlayFun",
            value: function() {
                var e;
                window.FLASH_MGTV_FUN && FLASH_MGTV_FUN.getNextPlayUrl && (e = FLASH_MGTV_FUN.getNextPlayUrl()) ? window.location.href = e : this.nextVideoInfo && this.nextVideoInfo.url && (window.location.href = this.nextVideoInfo.url)
            }
        }, {
            key: "showPlayTips",
            value: function(e) {
                var t = !(arguments.length > 1 && arguments[1] !== undefined) || arguments[1]
                  , i = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0
                  , o = this.$box.find("div[node-type=video-bottom-tips]");
                o.html(e).show(),
                i && this.$box.find("i[node-type=trailtime]").html(i),
                t && setTimeout(function() {
                    o.fadeOut("slow")
                }, 5e3)
            }
        }, {
            key: "hidePlayTips",
            value: function(e) {
                this.$box.find("div[node-type=video-bottom-tips]").fadeOut("slow")
            }
        }, {
            key: "showLoading",
            value: function() {
                var e = !(arguments.length > 0 && arguments[0] !== undefined) || arguments[0]
                  , t = this.$box.find(".alert-loading");
                e ? t.show() : t.hide()
            }
        }, {
            key: "getHistoryDef",
            value: function() {
                var e = {};
                try {
                    e = !!(e = $.LS.get("video-history-definition")) && JSON.parse(e)
                } catch (t) {}
                return e
            }
        }, {
            key: "getFrameImage",
            value: function(e, t) {
                var i = void 0
                  , o = void 0
                  , n = void 0
                  , s = void 0
                  , a = void 0;
                if (e.second && e.second[0] && e.images) {
                    i = (s = (o = e.second.join("|").split("|")).length) - 1;
                    for (var r = 0; r < s - 1; r++)
                        if (t >= o[r] && t < o[r + 1]) {
                            i = r;
                            break
                        }
                    return a = i % 100,
                    {
                        idx: n = Math.floor(i / 100),
                        index: i,
                        image: e.images[n],
                        x: a % 10 * 130,
                        y: 74 * Math.floor(a / 10)
                    }
                }
                return null
            }
        }, {
            key: "videoEventHandler",
            value: function(e) {
                "hlsSegmentLoaded" != e.type && console.log("videoEventHandler:", e.type);
                var t = void 0;
                switch (e.type) {
                case "hlsSegmentLoaded":
                    "fetchSource" == (t = e.detail.data).loader && o.segmentLoaded(t);
                    break;
                case "playinfo":
                    switch ((t = e.detail).states.status) {
                    case "loadComplete":
                        break;
                    case "loadError":
                        this.showError({
                            code: o.RequestErrorType.ERROR_CMS_FAIL.code,
                            msg: o.RequestErrorType.ERROR_CMS_FAIL.msg
                        });
                        break;
                    case "loadTimeout":
                        this.showError({
                            code: o.RequestErrorType.ERROR_CMS_TIMEOUT.code,
                            msg: o.RequestErrorType.ERROR_CMS_TIMEOUT.msg
                        })
                    }
                    break;
                case "getDispatcher":
                    switch ((t = e.detail).states.status) {
                    case "loadComplete":
                        o.requestSuccess({
                            type: o.RequestType.DISPATCHER,
                            h: t.states.url
                        });
                        break;
                    case "loadError":
                        this.showError({
                            code: o.RequestErrorType.ERROR_DISPATCHER_FAIL.code,
                            msg: o.RequestErrorType.ERROR_DISPATCHER_FAIL.msg
                        });
                        break;
                    case "loadTimeout":
                        this.showError({
                            code: o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                            msg: o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg
                        })
                    }
                    break;
                case "playStart":
                    o.requestStart({
                        type: o.RequestType.CDN
                    }),
                    $("div[node-type=loading-box]").hide();
                    break;
                case "bufferFull":
                    this.showLoading(!1),
                    o.playBufferFull(),
                    this.hasPlayStart || (t = e.detail,
                    o.requestSuccess({
                        type: o.RequestType.CDN,
                        h: t.states.url
                    }));
                    break;
                case "bufferEmpty":
                    o.playBufferEmpty();
                    break;
                case "playing":
                    this.hasPlayStart || (this.hasPlayStart = !0,
                    o.playStart(),
                    l.GET_PLAY_EVENT(),
                    l.SET_FLOATING_WINDOW("1"));
                    break;
                case "getSource":
                    switch ((t = e.detail).states.status) {
                    case "loadComplete":
                        o.requestSuccess({
                            type: o.RequestType.GETSOURCE,
                            h: t.states.url
                        });
                        break;
                    case "loadError":
                        o.requestError({
                            type: o.RequestType.GETSOURCE,
                            h: t.states.url
                        });
                    case "loadTimeout":
                        o.requestError({
                            type: o.RequestType.GETSOURCE,
                            h: t.states.url
                        })
                    }
                    break;
                case "pauseNotify":
                    this.hasPlayStart && o.playPause(),
                    l.SET_FLOATING_WINDOW_STATUS(0);
                    break;
                case "unpauseNotify":
                    this.hasPlayStart && o.playResume(),
                    l.SET_FLOATING_WINDOW_STATUS(1);
                    break;
                case "seekNotify":
                    console.log("state:", this.kernel.state, this.switching),
                    (2 == this.kernel.state || this.switching) && (this.kernel.playVideo(),
                    this.switching && (this.switching = !1));
                    break;
                case "playStop":
                    console.log("视频结束了", this.endVideo),
                    this.endVideo();
                    break;
                case "streamError":
                    t = e.detail,
                    console.log("streamError", t),
                    this.hasPlayStart = !1,
                    this.kernel && 1 == this.kernel.state && this.kernel.pauseVideo();
                    var i = o.RequestErrorType.ERROR_CDN_STREAM_NOT_FOUND.code
                      , n = o.RequestErrorType.ERROR_CDN_STREAM_NOT_FOUND.msg;
                    if (t.states) {
                        var s = t.states.error;
                        switch (s.type) {
                        case "hlsNetworkError":
                            i = o.RequestErrorType.ERROR_CDN_NETWORK_ERROR.code,
                            n = o.RequestErrorType.ERROR_CDN_NETWORK_ERROR.msg;
                            break;
                        case "hlsMediaError":
                            i = o.RequestErrorType.ERROR_CDN_MEDIA_ERROR.code,
                            n = o.RequestErrorType.ERROR_CDN_MEDIA_ERROR.msg;
                            break;
                        case "hlsOtherError":
                            i = o.RequestErrorType.ERROR_CDN_OTHER_ERROR.code,
                            n = o.RequestErrorType.ERROR_CDN_OTHER_ERROR.msg
                        }
                        switch (s.details) {
                        case "hlsMediaPlaylistLoadError":
                            i = "306001";
                            break;
                        case "hlsMediaPlaylistLoadTimeout":
                            i = "306002";
                            break;
                        case "hlsSegmentLoadTimeout":
                            i = "306004";
                            break;
                        case "hlsMediaError":
                            i = "307000",
                            s.code && (i = 307e3 + parseInt(s.code));
                            break;
                        case "hlsSegmentAppendingError":
                            i = "307005";
                            break;
                        case "hlsSegmentBufferFullError":
                            i = "307006";
                            break;
                        case "hlsSegmentParsingError":
                            i = "307007";
                            break;
                        case "hlsMediaPlaylistParseError":
                            i = "308001"
                        }
                        o.requestError({
                            type: o.RequestType.CDN,
                            h: t.states.url || "",
                            code: i,
                            z: "1",
                            error: s
                        })
                    }
                    this.showError({
                        code: i,
                        msg: n
                    }),
                    o.playStop();
                    break;
                case "log":
                    var a = {
                        h: (t = e.detail).states.url
                    };
                    "loadComplete" != t.states.status && (a.z = t.states.retry == t.states.maxRetry - 1 ? "1" : "0",
                    "loadError" == t.states.status ? "playinfo" == t.states.api ? (a.type = o.RequestType.CMS,
                    a.code = o.RequestErrorType.ERROR_CMS_FAIL.code,
                    a.msg = o.RequestErrorType.ERROR_CMS_FAIL.msg) : "getDispatcher" == t.states.api ? (a.type = o.RequestType.DISPATCHER,
                    a.code = o.RequestErrorType.ERROR_DISPATCHER_FAIL.code,
                    a.msg = o.RequestErrorType.ERROR_DISPATCHER_FAIL.msg) : "getSource" == t.states.api && (a.type = o.RequestType.GETSOURCE,
                    a.code = o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                    a.msg = o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg) : "loadTimeout" == t.states.status && ("playinfo" == t.states.api ? (a.type = o.RequestType.CMS,
                    a.code = o.RequestErrorType.ERROR_CMS_TIMEOUT.code,
                    a.msg = o.RequestErrorType.ERROR_CMS_TIMEOUT.msg) : "getDispatcher" == t.states.api ? (a.type = o.RequestType.DISPATCHER,
                    a.code = o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                    a.msg = o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg) : "getSource" == t.states.api && (a.type = o.RequestType.GETSOURCE,
                    a.code = o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.code,
                    a.msg = o.RequestErrorType.ERROR_DISPATCHER_TIMEOUT.msg)),
                    o.requestError(a))
                }
            }
        }]),
        u
    }();
    e.MGTVPlayer = u
});
