honey.def("lib:jquery", function(t) {
	var y = "application/x-shockwave-flash",
		b = {},
		v = {},
		s = {},
		I = window,
		x = document;
	window.openH5Player = !1;
	var e = ["94l6w226e", "94l6w226d", "94l6w226c", "94l6w226b", "94l6w226a", "94l6w2269", "94l6w2268", "94l6w226n", "94l6w226m", "95exclax3", "95exclax2", "95exclax1", "95exclax0", "95exclawz", "95exclawy", "95exclawx", "95exclaww", "95exclaxb", "95exclaxa", "95exclapz", "95exclapy", "95exclapx", "95exclapw", "95exclapv", "95exclapu", "95exclapt", "95exclaps", "95exclaq7", "95exclaq6", "95exclaiv", "90f12f8na", "90f12f8n9", "90f12f8n8", "90f12f8n7", "90f12f8n6", "90f12f8n5", "90f12f8n4", "90f12f8nj", "90f12f8ni", "90f945a4n", "90f945a4m", "90f945a4l", "90f945a4k", "90f945a4j", "90f945a4i", "90f945a4h", "90f945a4g", "90f945a4v", "90f945a4u", "90f9459xj", "90f9459xi", "90f9459xh", "90f9459xg", "90f9459xf", "90f9459xe", "90f9459xd", "90f9459xc", "90f9459xr", "90f9459xq", "90f9459qf", "90f9459qe", "90f9459qd", "90f9459qc", "90f9459qb", "90f9459qa", "90f9459q9", "90f9459q8", "90f9459qn", "90f9459qm", "90f945b47", "90f12gp6u", "90f12gp6t", "90f12gp6s", "90f12gp6r", "90f12gp6q", "90f12gp6p", "90f12gp6o", "90f12gp73", "90f12gp72", "90f8tgeiv", "90f8tgeiu", "90f8tgeit", "90f8tgeis", "90f8tgeir", "90f8tgeiq", "90f8tgeip", "90f8tgeio", "90f8tgej3", "90f8tgej2", "90f8tgebr", "90f8tgebq", "90f8tgebp", "90f8tgebo", "90f8tgebn", "90f8tgebm", "90f8tgebl", "90f8tgebk", "90f8tgebz", "90f8tgeby", "90f8tge4n", "90f8tge4m", "90f8tge4l", "90f8tge4k", "90f8tge4j", "90f8tge4i", "90f8tge4h", "90f8tge4g", "90f8tge4v", "90f8tge4u", "90f8tgfif", "90f62oqfm", "90f945b46", "90f945b45", "90f945b44", "90f945b43", "90f945b42", "90f945b41", "90f945b40", "90f945b4f", "90f945b4e", "90f945ax3", "90f945ax2", "90f945ax1", "90f945ax0", "90f945awz", "90f945awy", "90f945awx", "90f945aww", "90f945axb", "90f945axa", "90f945apz"];
	!
	function(e) {
		var i = e.userAgent,
			t = "undefined",
			a = "Shockwave Flash",
			o = /iPhone/i.test(i),
			n = /iPod/i.test(i),
			r = /iPad/i.test(i),
			d = o || r || n,
			l = typeof x.getElementById != t && typeof x.getElementsByTagName != t && typeof x.createElement != t,
			c = e.userAgent.toLowerCase(),
			s = e.platform.toLowerCase(),
			f = /win/.test(s || c),
			p = /mac/.test(s || c),
			u = !! /webkit/.test(c) && parseFloat(c.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")),
			v = !1,
			m = [0, 0, 0],
			w = null;
		if (typeof e.plugins != t && "object" == typeof e.plugins[a])!(w = e.plugins[a].description) || typeof e.mimeTypes != t && e.mimeTypes[y] && !e.mimeTypes[y].enabledPlugin || (v = !1, w = w.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), m[0] = parseInt(w.replace(/^(.*)\..*$/, "$1"), 10), m[1] = parseInt(w.replace(/^.*\.(.*)\s.*$/, "$1"), 10), m[2] = /[a-zA-Z]/.test(w) ? parseInt(w.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0);
		else if (typeof I.ActiveXObject != t) try {
			var h = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
			h && (w = h.GetVariable("$version")) && (v = !0, w = w.split(" ")[1].split(","), m = [parseInt(w[0], 10), parseInt(w[1], 10), parseInt(w[2], 10)])
		} catch (g) {}
		b = {
			w3: l,
			pv: m,
			wk: u,
			ie: v,
			win: f,
			mac: p,
			ios: d
		}
	}(navigator);
	for (var a = {
		width: "100%",
		height: "100%",
		autoplay: !1,
		html5Sources: "",
		mute: !1,
		volume: 50,
		modId: "mgtv-player-wrap",
		playerid: "hunantv-player-1",
		share: !0,
		flashWmode: "Opaque",
		playerVersion: "11.2.0",
		bullet: !1,
		vodFlashUrl: "//player.mgtv.com/mgtv_v5_main/main.swf"
	}, i = (v = {
		log: function(e) {
			if ("string" == typeof e)(new Image).src = e;
			else for (var i = 0; i < e.length; i++) {
				(new Image).src = e[i]
			}
		},
		stklog: function(e) {
			self.DATAREPORT || (self.DATAREPORT = new STK.start), self.DATAREPORT.pageStart(e)
		},
		cookieGet: function(e) {
			var i = new RegExp("(?:^|;+|\\s+)" + e + "=([^;]*)"),
				t = document.cookie.match(i);
			return t ? t[1] : ""
		},
		getUrlParam: function(e, i) {
			i = i || document.location.toString();
			var t, a = new RegExp("(^|&|\\\\?)" + e + "=([^&]*)(&|$|#)");
			return (t = i.match(a)) ? t[2] : ""
		},
		hasPlayerVersion: function(e) {
			var i = b.pv,
				t = e.split(".");
			return t[0] = parseInt(t[0], 10), t[1] = parseInt(t[1], 10) || 0, t[2] = parseInt(t[2], 10) || 0, i[0] > t[0] || i[0] == t[0] && i[1] > t[1] || i[0] == t[0] && i[1] == t[1] && i[2] >= t[2]
		},
		param: function(e) {
			var i = "";
			for (var t in e)"" != i && (i += "&"), i += t + "=" + encodeURIComponent(e[t]);
			return i
		},
		createFlashParam: function(e, i, t) {
			var a = [];
			return a = ['<param name="allowScriptAccess" value="always">', '<param name="quality" value="best" >', '<param name="allowFullScreen" value="true">', '<param name="wmode" value="' + e.flashWmode + '" >', '<param name="allowScriptAccess" value="always">', '<param name="flashvars" value="' + i + '">', '<param name="bgcolor" value="#000000">', '<param name="type" value="application/x-shockwave-flash" >', '<param name="pluginspage" value="https://www.macromedia.com/go/getflashplayer" >'], 1 == t && a.unshift('<param name="movie" value="' + e.vodFlashUrl + '">'), a.join("")
		},
		h5Report: function(e, i, t, a) {
			e.DATAREPORT || (e.DATAREPORT = new STK.start);
			var o = {
				url: window.location.href,
				time: STK.$.getTimer("yyyyMMddhhmmss"),
				suuid: STK.$.getSessionStorage("player_suuid"),
				ref: window.location.referrer || document.referrer,
				act: "hb",
				def: e.def,
				istry: "0",
				pay: e.videoinfo.info.paymark,
				idx: "0",
				pver: e.version,
				vts: e.videoinfo.info.duration,
				cid: VIDEOINFO.rid,
				plid: VIDEOINFO.cid || "",
				vid: VIDEOINFO.vid || "",
				ctl: VIDEOINFO.mod,
				pt: "0",
				cf: e.videoinfo.info.clip_type,
				cpn: VIDEOINFO.pt,
				bdid: VIDEOINFO.bdid || "",
				bsid: VIDEOINFO.sid,
				ct: "0",
				et: "0",
				td: "0",
				ap: e.isAutoPlay ? "1" : "0"
			};
			o = $.extend({}, o, i), e.DATAREPORT.pageStart(o, t, a)
		}
	}).getUrlParam("cxid"), o = 0; o < e.length; o++) if (e[o] === i) {
		window.BLACKTYPE = e[o];
		break
	}
	var n = function(e) {
			var i = this;
			i.version = "5.5.3", i.name = "芒果TV视频播放器", i.config = $.extend({}, a, e), i.box = $("#" + i.config.modId), i.adbox = $("#a_box"), i.$danmu = $('[node-type="c-player-pop-danmu"]'), i.reset(), i.Adv = t.INCOME && new t.INCOME, i.vipDialog = s = new t.vipDialog(".c-player-video"), $("div[node-type=loading-box]").hide()
		};
	n.prototype.reset = function() {
		var e = this;
		e.purview = !1, e.skipAd = !1, e.isAutoPlay = !1, e.queue = [], e.adTotalTime = 0, e.hasAdvertise = !1, e.isothercdn = !1, e.firstPlay = !0, e.playVVReported = !1, e.lastSeekEndTime = 0, e.lastBufferProcessed = !0, e.lastBufferTime = 0, e.lastBufferVideoTime = 0, e.h5ReportIndex = {
			buffer: 0,
			resume: 0,
			drag: 0,
			hb: 0
		}, clearTimeout(e.heartbeatCid), e.heartbeatCid = null, clearInterval(e.adCountDownTimer), e.adCountDownTimer = null, e.vipDialog && e.vipDialog.destroy(), e.player && e.player[0] && e.player[0].pause()
	}, n.prototype.localSave = v.localSave = function(e, i, t) {
		if (window.store && VIDEOINFO) {
			for (var a = window.store, o = VIDEOINFO.vid, n = [], r = 0, d = !0, l = a.get("honey-rec") || [], c = {
				updateTime: $.now(),
				video_id: o,
				vid: o,
				cid: VIDEOINFO.cid,
				url: window.location.origin + window.location.pathname,
				sid: VIDEOINFO.sid,
				video_name: VIDEOINFO.title,
				watchTime: e,
				time: i,
				isEnd: t
			}; l.length;) {
				var s = l.shift();
				if (s.video_id === o && (s = c, d = !1), n.push(s), 10 < ++r) break
			}
			d && n.unshift(c), a.set("honey-rec", n);
			for (var f = [], p = 0, u = !0, v = a.get("player-add") || [], m = {
				updateTime: $.now(),
				vid: o,
				cid: VIDEOINFO.cid,
				sid: VIDEOINFO.sid,
				watchTime: i,
				isEnd: t
			}; v.length;) {
				var w = v.shift();
				if (w.vid === o && (w = m, u = !1), f.push(w), 10 < ++p) break
			}
			u && f.unshift(m), a.set("player-add", f)
		}
	}, n.prototype.fetchVideoInfo = function(i) {
		var t, a = this,
			e = window.STK && window.STK.$.getCxid("cxid") || "",
			o = "//pcweb.api.mgtv.com/player/video?type=pad&video_id=" + a.config.vid + "&cxid=" + e + "&callback=?";
		$.ajax({
			url: o,
			dataType: "jsonp",
			method: "get",
			success: function(e) {
				e && e.data && 200 == e.code ? (a.videoinfo = t = e.data, t && 1 == t.info.isiplimit && (a.isiplimit = !0), t && 200 == t.user.purview && (a.purview = !0), 1 == t.user.isvip && (a.skipAd = !0), i && i(e)) : console.log("get videoinfo fail")
			}
		})
	}, n.prototype.fetchVideoSource = function(e, t) {
		var a = this,
			o = /(\w+):\/\/([^\:|\/]+)(\:\d*)?(.*\/)([^#|\?|\n]+)?(#.*)?(\?.*)?/i;
		$.getJSON(e + "&callback=?", function(e) {
			if (e && "ok" == e.status) {
				a.queue.push({
					source: e.info
				}), a.cdnip = "";
				try {
					a.cdnip = o.exec(e.info)[2]
				} catch (i) {}
				a.isothercdn = 0 !== e.isothercdn, t && t(e)
			} else console.log("get video source fail")
		})
	}, n.prototype.getDispatcher = function(e, i) {
		var t = "";
		return i = i || 0, e.stream_domain.length && e.stream.length && (t = e.stream_domain[i] + e.stream[i].url, this.def = e.stream[i].def), t
	}, n.prototype.createVideo = function() {
		var e, i = this,
			t = i.config;
		e = ['<div id="video_box">   <video id="' + t.playerid + '" type="" src=""  class="video video-js vjs-default-skin vjs-big-play-centered initPlayer" preload="true" width="' + t.width + '" heigt="' + t.height + '" autoplay="true"></video></div><div id="a_box">\t<div class="c-player-client-pad">\t\t<a href="#" class="u-install" data-pos="6" node-type="guide-app">安装芒果TV，享3倍流畅度</a>\t\t<a href="Javascript:;" class="u-play" id="playBtn">播放</a>\t</div>   <div id="cover"></div>   <div id="gg_box">       <span id="downCount">广告也精彩<i></i>秒</span>           <a target="_blank" href="" id="gg_link">       <span id="detials">了解详情&nbsp;&nbsp;&gt;</span>       </a>   </div></div>'].join(""), i.box.html(e), i.$player = $("#" + t.playerid), i.adbox = $("#a_box"), i.bindEvent(), window.__POSTMVPDATAIPAD = $.now(), v.stklog({
			bid: "14.1.91",
			edu: "ipad",
			act: "webpm",
			suuid: STK.$.getSessionStorage("player_suuid"),
			dom: i.$player.length,
			modl: VIDEOINFO.mod,
			vid: VIDEOINFO.vid,
			cid: VIDEOINFO.rid,
			ch: STK.$.getCxid("cxid") || "",
			os: STK.$.os.sys || "",
			browser: STK.$.browser.brName + " " + STK.$.browser.version
		})
	}, n.prototype.createSWF = function(e) {
		var i = this,
			t = (e = i.config, ""),
			a = [],
			o = "ok",
			n = 0,
			r = {
				video_id: e.vid
			},
			d = '<div id="floatingMasker"><div id="banner">按住视频可进行拖动</div><span class="scale"><em><i></i>点击缩小</em></span><span class="scale scale-big"><em><i></i>点击放大</em></span><span class="close"></span></div>';
		if ((t = $.extend({}, r, e.flashVars)).web_jump = 1, t = v.param(t) || "", a = b.ie ? [d + '<object id="' + e.playerid + '" name="" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="100%" height="100%"> ' + v.createFlashParam(e, t, 1) + "</object>"].join("") : [d + '<object id="' + e.playerid + '" data="' + e.vodFlashUrl + '" type="' + y + '" width="100%" height="100%"> ' + v.createFlashParam(e, t, 0) + "</object>"].join(""), i.box.css({
			height: e.height + "px",
			width: e.width + "px"
		}), i.box.html(a), i.$player = $("#" + e.playerid), i.$swf = document.getElementById(e.playerid), window.__POSTMVPDATAEND = $.now() + "|" + b.pv, window.__POSTMVPDATADOM = i.$player.length, !v.hasPlayerVersion(e.playerVersion)) {
			window.__POSTMVPDATAINSTALL = $.now(), o = "no", i.$player.append("<div>" + $("#mgtv_flash_install").html() + "</div>");
			var l = window.STK && window.STK.$.getCxid("cxid") || "",
				c = "//pcweb.api.mgtv.com/qrcode/?source=pc2&from=pcweb&p=" + (VIDEOINFO.vid || "") + "&plid=" + (VIDEOINFO.plid || "") + "&cxid=" + l;
			i.$player.find(".iconlist .code img").attr("src", c);
			try {
				window.FLASH_FLOATIONPLAYERWINDOW("2")
			} catch (u) {}
		}
		if (v.stklog({
			bid: "1.1.91",
			edu: "pcweb",
			act: "webpm",
			dom: i.$player.length,
			modl: VIDEOINFO.mod,
			suuid: STK.$.getSessionStorage("player_suuid"),
			vid: VIDEOINFO.vid,
			cid: VIDEOINFO.rid,
			swf: o,
			ch: STK.$.getCxid("cxid") || "",
			os: STK.$.os.sys || "",
			browser: STK.$.browser.brName + " " + STK.$.browser.version
		}), "ok" == o && 0 < i.$player.length) {
			var s, f = +new Date;
			var p = setInterval(function() {
				n++, !
				function(e) {
					try {
						return 100 == Math.floor(e.PercentLoaded())
					} catch (u) {
						return !1
					}
				}(i.$swf) ? 100 < n && (s = +new Date, v.stklog({
					bid: "1.1.91",
					edu: "pcweb",
					act: "flashload",
					dom: i.$player.length,
					modl: VIDEOINFO.mod,
					vid: VIDEOINFO.vid,
					cid: VIDEOINFO.rid,
					swf: o,
					fs: 0,
					lt: Math.max(s - f, 0),
					ch: STK.$.getCxid("cxid") || "",
					os: STK.$.os.sys || "",
					browser: STK.$.browser.brName + " " + STK.$.browser.version
				}), clearInterval(p), p = null) : (s = +new Date, v.stklog({
					bid: "1.1.91",
					edu: "pcweb",
					act: "flashload",
					dom: i.$player.length,
					modl: VIDEOINFO.mod,
					vid: VIDEOINFO.vid,
					cid: VIDEOINFO.rid,
					swf: o,
					fs: 1,
					lt: Math.max(s - f, 0),
					ch: STK.$.getCxid("cxid") || "",
					os: STK.$.os.sys || "",
					browser: STK.$.browser.brName + " " + STK.$.browser.version
				}), clearInterval(p), p = null)
			}, 100)
		}
		window.FlashObj = m("hunantv-player-1")
	}, n.prototype.autoPlayNext = function() {
		var e = "",
			i = this;
		if (window.FLASH_MGTV_FUN && window.FLASH_MGTV_FUN.getPlayerNextId && (e = window.FLASH_MGTV_FUN.getPlayerNextId()), e && e.url) return i.nextvideoinfo = e, void(window.location.href = e.url);
		if (e) {
			var t, a, o, n = !1;
			e && e.indexOf("&") ? (o = e.split("&")[0], n = "false" == e.split("&")[1]) : o = e, n && (t = VIDEOINFO.cid || "", a = VIDEOINFO.plid || "");
			var r = "//pcweb.api.mgtv.com/player/vinfo?video_id=" + o + "&cid=" + t + "&pid=" + a + "&cxid=" + (window.STK && window.STK.$.getCxid("cxid") || "") + "&callback=?";
			$.ajax({
				url: r,
				dataType: "jsonp",
				method: "get",
				success: function(e) {
					e && e.data && 200 == e.code && (i.nextvideoinfo = e.data, window.location.href = e.data.url)
				}
			})
		}
	}, n.prototype.bindEvent = function() {
		var r = this;
		r.heartbeatCid = null;
		var i = function() {
				var e = 0 == r.h5ReportIndex.hb ? 15 : 1 == r.h5ReportIndex.hb ? 30 : 2 == r.h5ReportIndex.hb ? 15 : 120;
				r.heartbeatCid = setTimeout(function() {
					var e = r.$player[0].currentTime;
					window.getHeartbeatEvent(e, r.videoinfo.info.duration), v.h5Report(r, {
						act: "hb",
						idx: r.h5ReportIndex.hb,
						ct: e,
						et: e,
						td: 0,
						ht: Math.min(r.h5ReportIndex.hb + 3, 6)
					}, "//padweb-v0.log.mgtv.com/hb.php", ["bid", "wuid", "rdc", "pix", "nfid", "ctl", "et"]), r.h5ReportIndex.hb = r.h5ReportIndex.hb + 1, i()
				}, 1e3 * e)
			},
			t = function() {
				r.heartbeatCid && clearTimeout(r.heartbeatCid)
			},
			a = function() {
				r.playVVReported || (v.h5Report(r, {
					act: "play",
					bid: "4.0.3",
					cver: "1.0.0",
					cdnip: r.cdnip || "",
					isad: r.hasAdvertise ? "1" : "0"
				}, "", ["rdc", "wuid"]), r.playVVReported = !0)
			};
		r.adbox.on("click", "#playBtn", function() {
			r.video && r.video.isAd ? (r.adbox.find("#cover").hide(), r.adbox.find(".c-player-client-pad").hide(), r.adbox.find("#gg_box").show()) : r.adbox.hide(), r.$player[0].play(), r.$player.removeClass("initPlayer")
		}), r.adbox.on("click", "#gg_link", function() {
			v.log(r.video.linkTrack)
		}), r.$player.on("play", function() {
			if (r.video.isAd);
			else {
				if (!r.isPaused) {
					var e = r.$player[0].currentTime;
					window.getPlayEvent(e, r.videoinfo.info.duration)
				}
				a(), i(), r.isPaused = !1
			}
		}), r.$player.on("playing", function() {
			if (r.video.isAd) {
				if (r.adbox.find("#cover").hide(), r.adbox.find(".c-player-client-pad").hide(), r.adbox.find("#gg_box").show(), v.log(r.video.impress), "" == r.video.link ? r.adbox.find("#detials").hide() : (r.adbox.find("#detials").show(), r.adbox.find("#gg_link").attr("href", r.video.link)), r.firstPlay) {
					r.firstPlay = !1;
					var i = r.adbox.find("#downCount i").html(r.adTotalTime);
					r.adCountDownTimer = setInterval(function() {
						var e = parseInt(i.html());
						e <= 0 ? clearInterval(r.adCountDownTimer) : i.html(e - 1)
					}, 1e3)
				}
			} else if (a(), r.adbox.hide(), r.adbox.find("#cover").hide(), r.adbox.find("#gg_box").hide(), r.adbox.find(".c-player-client-pad").hide(), !r.lastBufferProcessed) {
				var e = +new Date;
				v.h5Report(r, {
					act: "buffer",
					idx: r.h5ReportIndex.buffer,
					bftype: r.lastBufferType,
					ct: r.lastBufferVideoTime,
					et: r.$player[0].currentTime,
					td: Math.max(0, e - r.lastBufferTime)
				}, "//padweb-v0.log.mgtv.com/buffer.php", ["bid", "wuid", "rdc", "ctl", "et", "nfid", "pix"]), r.h5ReportIndex.buffer = r.h5ReportIndex.buffer + 1, r.lastBufferProcessed = !0
			}
		}), r.$player.on("timeupdate", function() {}), r.$player.on("seeking", function() {
			r.lastSeekEndTime = +new Date
		}), r.$player.on("seeked", function() {
			if (!r.video.isAd) {
				var e = r.$player[0].currentTime;
				window.getSeekEvent(e, r.videoinfo.info.duration), r.lastSeekEndTime = +new Date
			}
		}), r.$player.on("resume", function() {
			r.video.isAd || i()
		}), r.$player.on("loadedmetadata", function() {
			r.video.isAd
		}), r.$player.on("waiting", function() {
			r.video.isAd || (r.lastBufferTime = +new Date, r.lastBufferVideoTime = r.$player[0].currentTime, r.lastBufferProcessed = !1, r.lastBufferType = 0 == r.h5ReportIndex.buffer ? 1 : 2, Math.abs(r.lastSeekEndTime - r.lastBufferTime) < 1e3 && (r.lastBufferType = 3))
		}), r.$player.on("pause", function() {
			r.video.isAd || (r.isPaused = !0, t())
		}), r.$player.on("ended", function() {
			if (r.video.isAd) r.doPlay();
			else {
				var e = r.$player[0].currentTime;
				window.getStopEvent(e, r.videoinfo.info.duration), t(), r.config.web_jump ? (window.getPlayNextEvent && window.getPlayNextEvent(), window.refreshPage && window.refreshPage()) : r.autoPlayNext()
			}
		}), r.$player.on("error", function() {
			r.video && r.video.isAd
		}), $(document).on("HONEY-LOGIN-SUCCESS", function(e, i) {
			1 == v.cookieGet("vipStatus") && r.create({
				rebuild: !0
			})
		}), $(document).on("touchstart", "[node-type=guide-app]", function(e) {
			var i, t, a, o, n = $(this).data("pos");
			o = r.video && r.video.isAd ? 0 : r.$player[0].currentTime, i = VIDEOINFO.cid || "", t = VIDEOINFO.bdid || "", a = VIDEOINFO.vid || "", urlappBottom = "https://mtest.api.hunantv.com/download.html?from=pcweb&start_time=" + o + "&clipId=" + i + "&plId=" + t + "&videoId=" + a + "&type=1&pos=pc" + n, $("a[node-type=guide-app]").attr("href", urlappBottom), v.stklog({
				bid: b.ios ? "4.1.2" : "1.5.5.5",
				act: "click",
				pos: n
			})
		})
	}, n.prototype.doPlay = function() {
		var e, i = this;
		i.video = i.queue.shift(), i.$player.attr({
			src: i.video.source,
			type: "application/x-mpegurl"
		}), i.video.isAd ? i.adbox.show() : (e = i.videoinfo.info.thumb || "//honey.mgtv.com/hunantv.imgotv/image/player/loading.jpg", i.$player.attr({
			controls: "controls",
			poster: e
		}).removeClass("initPlayer"))
	}, n.prototype.create = function(e) {
		var a = this,
			i = 1,
			t = parseInt(v.getUrlParam("start_time"));
		if (e.rebuild || isNaN(t) || (e.flashVars.start_time = t), $.extend(a.config, e), b.ios) a.config.web_jump = 1, !e.rebuild && a.createVideo(), a.fetchVideoInfo(function(e) {
			if (a.isiplimit) a.box.html('<div style="color: white;font-size: 16px;text-align: center;margin-top: 200px;">因版权原因，本片只允许在中国大陆地区播放<br />如您在中国大陆地区观看受限，<a style="color:#f02600;" href="//i.mgtv.com/feedback.html" target="_blank">点击反馈</a></div>');
			else {
				if (!a.purview) return a.adbox.hide(), a.$danmu.hide(), void a.vipDialog.open(a.videoinfo);
				if (a.skipAd) {
					var i = a.getDispatcher(a.videoinfo);
					a.fetchVideoSource(i, function() {
						a.doPlay()
					})
				} else a.Adv.fetchAd(function(e) {
					a.adTotalTime = e.totalTime, a.queue = a.queue.concat(e.ads), a.hasAdvertise = 0 < e.ads.length;
					var i = a.getDispatcher(a.videoinfo);
					a.fetchVideoSource(i, function() {
						a.doPlay()
					})
				})
			}
		}), i = 6;
		else if (e.rebuild) {
			$.extend(a.config, {
				flashVars: {
					statistics_bigdata_bid: 1,
					black_type: window.BLACKTYPE ? window.BLACKTYPE : "",
					cxid: STK.$.getCxid("cxid"),
					wver: STK.$.getWver(),
					web_jump: 1,
					cpn: VIDEOINFO.cpn || "",
					video_id: VIDEOINFO.vid,
					cid: VIDEOINFO.cid || "",
					plid: VIDEOINFO.plid || "",
					video_bdid: VIDEOINFO.bdid || "",
					href: window.location.href,
					ref: location.referrer || document.referrer
				}
			});
			var o = v.param(a.config.flashVars) || "";
			a.$player.find('param[name="flashvars"]').attr({
				value: o
			}), $("#" + a.config.modId).find("#" + a.config.playerid).show(), $("#" + a.config.modId).find(".c-player-paytips").hide();
			try {
				a.$swf.playVideo(a.config.flashVars)
			} catch (r) {
				console.log("call swf [playVideo] fail:", r)
			}
		} else a.createSWF();
		if (e.rebuild ||
		function(a) {
			var o = function() {
					return "//playhistory.bz.mgtv.com/playHistory/heartbeat?pid=&ticket=" + i + "&uid=" + e + "&cid=" + VIDEOINFO.cid + "&vid=" + VIDEOINFO.vid + "&sid=" + VIDEOINFO.sid + "&from=1"
				},
				e = v.cookieGet("uuid"),
				i = "" == v.cookieGet("HDCN") ? "" : v.cookieGet("HDCN").match(/\b\w+\b/g)[0];
			if ("" !== v.cookieGet("HDCN")) {
				var t = window.store.get("player-add") || [],
					n = $("#mgtv-addplaylist-iframe"),
					r = $("#mgtv-addplaylist-from");
				if (t.length) {
					if (0 == n.length && $('<iframe frameborder="0" height="0" name="mgtv-addplaylist-iframe"  id="mgtv-addplaylist-iframe" width="0"/>').appendTo("body"), 0 == r.length) {
						var d = '<form action="//playhistory.bz.mgtv.com/playHistory/add" target="mgtv-addplaylist-iframe" method="post" id="mgtv-addplaylist-from"><input type="hidden" name="pid" value="" /><input type="hidden" name="ticket" value="' + i + '" /><input type="hidden" name="uid" value="' + e + '" /><input type="hidden" name="from" value="1" /><input type="hidden" name="playList" value="" /></form>';
						r = $(d).appendTo("body"), $('[name="playList"]', r).val(JSON.stringify(t)), r.submit()
					}
					window.store.set("player-add", [])
				}
			}
			window.getStopEvent = function(e) {
				window.USER ? honey.load(o() + "&watchTime=" + parseInt(0) + "&isEnd=1&callback=playerBack") : v.localSave("已观看100%", 0, 1)
			}, window.getSeekEvent = window.getHeartbeatEvent = function(e, i) {
				window.seekTime = e;
				var t = "已观看" + Math.ceil(100 * e / i) + "%";
				v.cookieGet("uuid");
				window.USER ? honey.load(o() + "&watchTime=" + parseInt(e) + "&isEnd=0&callback=playerBack") : v.localSave(t, e, 0)
			}, window.getPlayEvent = function(e, i) {
				var t = e || 0;
				window.seekTime = t, window.USER ? honey.load(o() + "&watchTime=" + parseInt(t) + "&isEnd=0&callback=playerBack") : v.localSave("已观看0%", 0, 0)
			}, window.showPlayPayDialog = function(e, i) {
				var t = {
					user: e,
					info: i
				};
				return $("#" + a.modId).find("#" + a.playerid).hide(), 200 != t.user.purview && (window.FLASH_FLOATIONPLAYERWINDOW && window.FLASH_FLOATIONPLAYERWINDOW(0), $('[node-type="c-player-pop-danmu"]').hide(), s.open(t)), !0
			}, window.getVideoInfo = function() {
				return window.VIDEOINFO
			}, window.playerBack = function() {}, window.getGUID = function() {
				var e = function(e) {
						var i = location.search,
							t = {};
						if (-1 != i.indexOf("?")) for (var a = i.substr(1), o = 0, n = a.split("&"); o < n.length; o += 1) t[n[o].split("=")[0]] = unescape(n[o].split("=")[1]);
						var r = t[e.toLowerCase()];
						return void 0 === r ? "" : r
					}("from"),
					i = function(e) {
						var i = new RegExp("(?:^|;+|\\s+)" + e + "=([^;]*)"),
							t = document.cookie.match(i);
						return t ? t[1] : ""
					},
					t = document.referrer,
					a = "" == i("MQGUID") ? i("__MQGUID") : i("MQGUID");
				return "" == t && "wb" == e && (t = "http://www.weibo.com"), a + "|" + t
			}, window.requestFullScreen = function() {
				if (fullScreenApi.supportsFullScreen) {
					var e = $("#mgtv-player-wrap");
					return fullScreenApi.requestFullScreen(e[0]), window.openH5Player = !0, 1
				}
				return 0
			}, window.exitFullScreen = function() {
				if (fullScreenApi.supportsFullScreen) {
					var e = $("#mgtv-player-wrap");
					return fullScreenApi.cancelFullScreen(e[0]), window.openH5Player = !1, 1
				}
				return 0
			}, window.onresize = function() {
				if (!
				function() {
					var e = document.fullscreenEnabled || window.fullScreen || document.webkitIsFullScreen || document.msFullscreenEnabled;
					e === undefined && (e = !1);
					return e
				}() && window.openH5Player) try {
					window.FlashObj.fullscreenCallback()
				} catch (e) {
					console.log(e)
				}
			}, window.onbeforeunload = function() {
				var e = document.getElementById("hunantv-player-1");
				e.PageClose && e.PageClose()
			};
			var l = null;
			window.FLASH_FLASHERROR_DATA = function() {
				c()
			};
			var c = function() {
					clearTimeout(l), l = setTimeout(function() {
						(new Image).src = "//pcweb-v1.log.mgtv.com/dispatcher.do?bid=1.1.91&act=flashcrash&modl=" + VIDEOINFO.mod
					}, 7e3)
				}
		}(a.config), v.log("//vc.mgtv.com/v2/watch?vid=" + VIDEOINFO.vid + "&cid=" + VIDEOINFO.cid + "&pid=&invoker=" + i), !e.rebuild) {
			var n = $(document);
			n.on("play.video.reload", function(e, i) {
				var t = a.config;
				t.vid = VIDEOINFO.vid, t.rebuild = !0, t.flashVars.start_time = 0, a.reportCP1(), a.reset(), a.isAutoPlay = !0, a.create(a.config), n.trigger("page.reload.mock", i)
			})
		}
	}, n.prototype.reportCP1 = function() {
		this.DATAREPORT = this.DATAREPORT || STK && new STK.start;
		var e = {
			act: "cp1",
			url: window.location.href,
			bid: "isIpad" == STK.$.os.sys ? "14.1.90" : "1.1.90",
			cid: VIDEOINFO.rid || "",
			vid: VIDEOINFO.vid || "",
			bdid: VIDEOINFO.bdid || "",
			mp: 1,
			idx: 0,
			ref: window.location.referrer || document.referrer,
			suuid: STK.$.setSessionStorage("player_suuid")
		};
		this.DATAREPORT.pageStart(e)
	}, window.MVP = {
		Player: n
	};
	var m = function(e) {
			var i = document.getElementById(e);
			return -1 != navigator.appName.indexOf("Microsoft") ? i : 0 < i.getElementsByTagName("embed").length ? i.getElementsByTagName("embed")[0] : i
		}
}), function(e) {
	var i = {
		supportsFullScreen: !1,
		isFullScreen: function() {
			return !1
		},
		requestFullScreen: function() {},
		cancelFullScreen: function() {},
		fullScreenEventName: "",
		prefix: ""
	},
		t = "webkit moz o ms khtml".split(" ");
	if ("undefined" != typeof document.cancelFullScreen) i.supportsFullScreen = !0;
	else for (var a = 0, o = t.length; a < o; a++) if (i.prefix = t[a], "undefined" != typeof document[i.prefix + "CancelFullScreen"]) {
		i.supportsFullScreen = !0;
		break
	}
	i.supportsFullScreen && (i.fullScreenEventName = i.prefix + "fullscreenchange", i.isFullScreen = function() {
		switch (this.prefix) {
		case "":
			return document.fullScreen;
		case "webkit":
			return document.webkitIsFullScreen;
		default:
			return document[this.prefix + "FullScreen"]
		}
	}, i.requestFullScreen = function(e) {
		e.requestFullscreen ? e.requestFullscreen() : e.mozRequestFullScreen ? e.mozRequestFullScreen() : e.webkitRequestFullScreen && e.webkitRequestFullScreen()
	}, i.cancelFullScreen = function() {
		document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen()
	}), e.fullScreenApi = i
}(window);